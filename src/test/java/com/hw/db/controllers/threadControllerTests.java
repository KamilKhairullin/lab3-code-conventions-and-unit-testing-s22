package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Thread;
import com.hw.db.models.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;

class threadControllerTests {

    private threadController controller;
    private User user;
    private Thread thread;
    private Post post;
    private Forum toCreate;
    private Vote vote;

    @BeforeEach
    @DisplayName("Thread Controller Tests")
    void setUp() {
        Timestamp current = Timestamp.from(Instant.now());
        controller = new threadController();
        user = new User("username", "username@mail.com", "User Name", "Hello!");

        thread = new Thread(user.getNickname(), current, "forum", "I am thread", "slug", "This is thread", 0);
        post = new Post(user.getNickname(), current, thread.getForum(), thread.getMessage(), 0, 0, false);
        vote = new Vote(user.getNickname(), 1);

        thread.setId(0);
    }

    @Test
    @DisplayName("Correct checkIdOrSlug test")
    public void checkIdOrSlugTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            Thread threadTest = controller.CheckIdOrSlug("0");
            assertThat(threadTest).isEqualTo(thread);
            threadDAO.verify(() -> ThreadDAO.getThreadById(0));
        }
    }

    @Test
    @DisplayName("Correct post creation test")
    void correctlyCreatePost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(thread);

            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info("username"))
                        .thenReturn(user);

                List<Post> posts = List.of(post);
                controller.createPost(thread.getSlug(), posts);
                threadDaoMock.verify(() -> ThreadDAO.createPosts(thread, posts, List.of(user)));
            }
        }
    }


    @Test
    @DisplayName("Correctly get posts tests")
    public void correctlyGetPostsTests() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadDAO.when(() -> ThreadDAO.getPosts(eq(0), eq(1), eq(0), eq("flat"), eq(false)))
                    .thenReturn(List.of(post));
            ResponseEntity res = controller.Posts("slug", 1, 0, "flat", false);
            ResponseEntity expectedRes = ResponseEntity.status(HttpStatus.OK).body(List.of(post));
            assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);
        }
    }


    @Test
    @DisplayName("Correctly change thread test")
    void correctlyChangesThread() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            Thread otherThread = new Thread("username2", Timestamp.valueOf("1970-01-02 00:00:00"), "username2", "username2", "slug2", "username2", 0);
            otherThread.setId(1);

            threadMock.when(() -> ThreadDAO.getThreadBySlug("otherSlug")).thenReturn(otherThread);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(otherThread);
            threadMock.when(() -> ThreadDAO.change(thread, otherThread)).thenAnswer((_smth) -> {
                threadMock.when(() -> ThreadDAO.getThreadBySlug("slug2")).thenReturn(otherThread);
                return null;
            });

            assertEquals(controller.change("otherSlug", otherThread), ResponseEntity.status(HttpStatus.OK).body(otherThread));
        }
    }


    @Test
    @DisplayName("Correct thread info test")
    public void infoTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            ResponseEntity res = controller.info("slug");
            assertThat(res).isEqualToComparingFieldByField(ResponseEntity.status(HttpStatus.OK).body(thread));
        }
    }


    @Test
    @DisplayName("Correctly create vote test")
    void correctlyCreateVote() {
        Vote vote = new Vote("username", 0);

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info("username")).thenReturn(user);

                threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                threadMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());

                assertEquals(controller.createVote("slug", vote), ResponseEntity.status(HttpStatus.OK).body(thread));
            }
        }
    }
}

